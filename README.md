# Keithley

A simple python application based on PyFD that reads values from a Keithley
multimeter and saves them to frame data.

## Configuration keys

### KEITHLEY_SERIAL_NUMBER <sn>

Define the multimeter serial number (default:"8015476").

### KEITHLEY_CHANNEL_NAME <name>

Define the channel name used for the storage in the frame data (default:"KeithleyVoltage").

## USB access right configuration

udev must be configured to let the application access the multimeter via the USB
bus. Create a `/etc/udev/rules.d/usbtmc.rules/etc/udev/rules.d/usbtmc.rules`
with the following content:

```
# USBTMC instruments

# Keythley 2100
SUBSYSTEMS=="usb", ACTION=="add", ATTRS{idVendor}=="05e6", ATTRS{idProduct}=="2100", GROUP="usbtmc", MODE="0660"

# Devices
KERNEL=="usbtmc/*",       MODE="0660", GROUP="usbtmc"
KERNEL=="usbtmc[0-9]*",   MODE="0660", GROUP="usbtmc"
```

A `usbtmc` group must be created. The user running the application should be a
member of the `usbtmc` group.

For example, in `/etc/group`:

```
usbtmc:x:1000:virgoo
```
