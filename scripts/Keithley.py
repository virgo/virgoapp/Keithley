#!/usr/bin/env python3
import sys
import time
import logging
import math
import signal
import ctypes

import usbtmc

import PyFd as fd
import numpy as np

from quantiphy import Quantity

KEITHLEY_DT = 1
KEITHLEY_N_DATA = 5
KEITHLEY_SERIAL_NUMBER = b"8015476"
KEITHLEY_CHANNEL_NAME = b"KeithleyVoltage"

class GracefulInterruptHandler(object):

    def __init__(self, sig=signal.SIGINT):
        self.sig = sig

    def __enter__(self):

        self.interrupted = False
        self.released = False

        self.original_handler = signal.getsignal(self.sig)

        def handler(signum, frame):
            self.release()
            self.interrupted = True

        signal.signal(self.sig, handler)

        return self

    def __exit__(self, type, value, tb):
        self.release()

    def release(self):

        if self.released:
            return False

        signal.signal(self.sig, self.original_handler)

        self.released = True

        return True

def main(args=None):
    Quantity.set_prefs(spacer='')

    argv = sys.argv
    argc = len(argv)

    LP_c_char = ctypes.POINTER(ctypes.c_char)
    LP_LP_c_char = ctypes.POINTER(LP_c_char)

    p = (LP_c_char*len(argv))()
    for i, arg in enumerate(argv):
        enc_arg = arg.encode('ascii')
#        print (enc_arg)
        p[i] = ctypes.create_string_buffer(enc_arg)

    c_argv = ctypes.cast(p, LP_LP_c_char)

    serial_number = ctypes.c_char_p(KEITHLEY_SERIAL_NUMBER)
    channel_name = ctypes.c_char_p(KEITHLEY_CHANNEL_NAME)

    fdio = fd.FdIONew(argc, c_argv)
    fd.CfgParseAdd(fdio.contents.parser, "KEITHLEY_SERIAL_NUMBER", 1, fd.CfgString, ctypes.byref(serial_number))
    fd.CfgParseAdd(fdio.contents.parser, "KEITHLEY_CHANNEL_NAME", 1, fd.CfgString, ctypes.byref(channel_name))
    fd.FdIOParseAndIni(fdio)

    try:
        multi=usbtmc.Instrument("USB::0x05e6::0x2100::{0}::INSTR".format(serial_number.value.decode()))
    except:
        fd.CfgMsgAddError("Keithley multimeter S/N:{0} not found".format(serial_number.value.decode()))
        sys.exit()

    fd.CfgReachState(fd.CfgServerConfigured)
    fd.CfgReachState(fd.CfgServerGolden)

    # Start next second
    start_gps = int(fd.FrGetCurrentGPS()) + 1
    count = 0
    data = np.empty (KEITHLEY_N_DATA)

    with GracefulInterruptHandler() as h:
        while not fd.CfgFinished() and not h.interrupted:

            sum = 0
            minimum = 0
            maximum = 0
            gps = start_gps + KEITHLEY_DT * count
            for i in range(KEITHLEY_N_DATA):
                next_gps = gps + i * KEITHLEY_DT / KEITHLEY_N_DATA
                current_gps = fd.FrGetCurrentGPS()
                sleep_time = next_gps - current_gps
                if (sleep_time > 0):
                    time.sleep (sleep_time)
                data[i] = multi.ask("meas?")
                sum += data[i]
                if i == 0:
                    minimum = sum
                    maximum = sum
                else:
                    if minimum > data[i]:
                        minimum = data[i]
                    if maximum < data[i]:
                        maximum = data[i]

            count += 1

            c_frame = fd.FrameHNew(fd.CfgGetCmName())
            c_frame.contents.GTimeS = int(gps)
            c_frame.contents.GTimeN = 1000000000 * (gps - int(gps))
            c_frame.contents.dt = KEITHLEY_DT

            c_adc = fd.FrAdcDataNew(c_frame, channel_name.value, KEITHLEY_N_DATA / KEITHLEY_DT, KEITHLEY_N_DATA, -32)
            c_adc.contents.units.contents = bytes("V", "ascii")
            c_data = c_adc.contents.data.contents.dataF
            array = np.asarray(data, dtype=np.float32)
            ctypes.memmove(c_data, array.ctypes.data, array.nbytes)

            if KEITHLEY_N_DATA > 1:
                fd.CfgMsgAddUserInfo(
                        f'GPS:{gps} - mean:{Quantity(sum / KEITHLEY_N_DATA, "V")} - '
                        f'min:{Quantity(minimum, "V")} - '
                        f'max:{Quantity(maximum, "V")}')
            else:
                fd.CfgMsgAddUserInfo(f'GPS:{gps} - value:{Quantity(sum / KEITHLEY_N_DATA, "V")}')

            fd.FdIOPutFrame(fdio, c_frame)

if __name__ == '__main__':
    sys.exit(main())
